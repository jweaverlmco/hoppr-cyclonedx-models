# Generate hoppr-cyclonedx-models

## Purpose

The `model-gen.sh` script pulls the latest tagged schema releases from the
[CycloneDX specification project][cyclonedx-spec] that operates under the SBOM guidelines. It then
utilizes the [datamodel code generator][datamodel-code-generator-docs] to generate Pydantic models
from the input SBOM schema file. These Pydantic models are available as a public package in PyPI
under `hoppr_cyclonedx_models`.

## Usage

### Install `datamodel-code-generator`

-----------------------------------------

```shell
pip install datamodel-code-generator
```

For further reference see the [documentation][datamodel-code-generator-docs].

### Create your models

-----------------------------------------

```shell
./model-gen.sh
```

To see all current releases visit [CycloneDX Specification Releases][cyclonedx-spec-releases].

[cyclonedx-spec]: https://github.com/CycloneDX/specification
[cyclonedx-spec-releases]: https://github.com/CycloneDX/specification/releases
[datamodel-code-generator-docs]: https://koxudaxi.github.io/datamodel-code-generator
