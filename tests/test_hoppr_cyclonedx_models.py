"""
Roundtrip SBOM parsing tests
"""
from __future__ import annotations

import json

from pathlib import Path
from typing import Callable, Literal

import pytest

from packaging.version import InvalidVersion
from packaging.version import parse as parse_version

from hoppr_cyclonedx_models import Sbom, __version__
from hoppr_cyclonedx_models.cyclonedx_1_3 import CyclonedxSoftwareBillOfMaterialSpecification as Bom_1_3
from hoppr_cyclonedx_models.cyclonedx_1_4 import CyclonedxSoftwareBillOfMaterialsStandard as Bom_1_4
from hoppr_cyclonedx_models.cyclonedx_1_5 import CyclonedxSoftwareBillOfMaterialsStandard as Bom_1_5


@pytest.fixture(name="roundtrip")
def roundtrip_test() -> Callable[[str, Literal["1.3", "1.4", "1.5"]], None]:
    """
    Fixture to return roundtrip test callable
    """

    def _roundtrip_test(filename: str, spec_version: Literal["1.3", "1.4", "1.5"]):
        data_dir = Path(__file__).parent / "data" / "cyclonedx_python_lib_fixture" / "json" / spec_version
        model_cls = {"1.3": Bom_1_3, "1.4": Bom_1_4, "1.5": Bom_1_5}[spec_version]

        # Load JSON file
        sbom_file = data_dir / filename
        original_dict = json.loads(sbom_file.read_text(encoding="utf-8"))

        # Parse loaded JSON data into model
        model_object = model_cls(**original_dict)
        assert isinstance(hash(model_object), int)

        # Export parsed model JSON string
        parsed_json_from_model = model_object.json(exclude_unset=True, by_alias=True)

        # Load parsed model JSON string
        parsed_dict_from_model = json.loads(parsed_json_from_model)

        # Assert equal
        assert original_dict == parsed_dict_from_model
        assert model_object == model_cls.parse_obj(parsed_dict_from_model)  # type: ignore[attr-defined]

    return _roundtrip_test


@pytest.mark.parametrize(
    argnames="filename",
    argvalues=[
        pytest.param("bom_dependencies.json", id="dependencies"),
        pytest.param("bom_dependencies_component.json", id="dependencies_component"),
        pytest.param("bom_external_references.json", id="external_references"),
        pytest.param("bom_issue_275_components.json", id="issue_275_components"),
        pytest.param("bom_services_complex.json", id="services_complex"),
        pytest.param("bom_services_nested.json", id="services_nested"),
        pytest.param("bom_services_simple.json", id="services_simple"),
        pytest.param("bom_setuptools.json", id="setuptools"),
        pytest.param("bom_setuptools_complete.json", id="setuptools_complete"),
        pytest.param("bom_setuptools_no_version.json", id="setuptools_no_version"),
        pytest.param("bom_setuptools_with_cpe.json", id="setuptools_with_cpe"),
        pytest.param("bom_toml_1.json", id="toml"),
        pytest.param("bom_with_full_metadata.json", id="full_metadata"),
    ],
)
def test_spec_1_3_roundtrip(filename: str, roundtrip: Callable[[str, Literal["1.3", "1.4", "1.5"]], None]):
    """
    Roundtrip test: Json -> CycloneDx 1.3 spec version Object -> Json
    """
    roundtrip(filename, "1.3")


@pytest.mark.parametrize(
    argnames="filename",
    argvalues=[
        pytest.param("bom_dependencies.json", id="dependencies"),
        pytest.param("bom_dependencies_component.json", id="dependencies_component"),
        pytest.param("bom_external_references.json", id="external_references"),
        pytest.param("bom_issue_275_components.json", id="issue_275_components"),
        pytest.param("bom_services_complex.json", id="services_complex"),
        pytest.param("bom_services_nested.json", id="services_nested"),
        pytest.param("bom_services_simple.json", id="services_simple"),
        pytest.param("bom_setuptools.json", id="setuptools"),
        pytest.param("bom_setuptools_complete.json", id="setuptools_complete"),
        pytest.param("bom_setuptools_no_version.json", id="setuptools_no_version"),
        pytest.param("bom_setuptools_with_cpe.json", id="setuptools_with_cpe"),
        pytest.param("bom_setuptools_with_release_notes.json", id="setuptools_with_release_notes"),
        pytest.param("bom_setuptools_with_vulnerabilities.json", id="setuptools_with_vulnerabilities"),
        pytest.param("bom_toml_1.json", id="toml"),
        pytest.param("bom_with_full_metadata.json", id="full_metadata"),
    ],
)
def test_spec_1_4_roundtrip(filename: str, roundtrip: Callable[[str, Literal["1.3", "1.4", "1.5"]], None]):
    """
    Roundtrip test: Json -> CycloneDx 1.4 spec version Object -> Json
    """
    roundtrip(filename, "1.4")


@pytest.mark.parametrize(
    argnames="filename",
    argvalues=[
        pytest.param("bom_dependencies.json", id="dependencies"),
        pytest.param("bom_dependencies_component.json", id="dependencies_component"),
        pytest.param("bom_external_references.json", id="external_references"),
        pytest.param("bom_issue_275_components.json", id="issue_275_components"),
        pytest.param("bom_services_complex.json", id="services_complex"),
        pytest.param("bom_services_nested.json", id="services_nested"),
        pytest.param("bom_services_simple.json", id="services_simple"),
        pytest.param("bom_setuptools.json", id="setuptools"),
        pytest.param("bom_setuptools_complete.json", id="setuptools_complete"),
        pytest.param("bom_setuptools_no_version.json", id="setuptools_no_version"),
        pytest.param("bom_setuptools_with_cpe.json", id="setuptools_with_cpe"),
        pytest.param("bom_setuptools_with_release_notes.json", id="setuptools_with_release_notes"),
        pytest.param("bom_setuptools_with_vulnerabilities.json", id="setuptools_with_vulnerabilities"),
        pytest.param("bom_toml_1.json", id="toml"),
        pytest.param("bom_with_full_metadata.json", id="full_metadata"),
    ],
)
def test_spec_1_5_roundtrip(filename: str, roundtrip: Callable[[str, Literal["1.3", "1.4", "1.5"]], None]):
    """
    Roundtrip test: Json -> CycloneDx 1.5 spec version Object -> Json
    """
    roundtrip(filename, "1.5")


@pytest.mark.parametrize(argnames="spec_version", argvalues=["1.3", "1.4", "1.5"])
def test_sbom_model(spec_version: str):
    """
    Verify hoppr_cyclonedx_models.Sbom model validation
    """
    sbom_file = (
        Path(__file__).parent
        / "data"
        / "cyclonedx_python_lib_fixture"
        / "json"
        / spec_version
        / "bom_issue_275_components.json"
    )

    sbom = Sbom.parse_file(sbom_file)
    assert isinstance(sbom, Sbom)
    assert sbom.specVersion == "1.5"
    assert sbom.field_schema == "http://cyclonedx.org/schema/bom-1.5.schema.json"
    assert isinstance(hash(sbom), int)


def test_version():
    """
    Verify valid version string
    """
    try:
        parse_version(__version__)
    except InvalidVersion:
        pytest.fail(f"Invalid version string: '{__version__}'")
